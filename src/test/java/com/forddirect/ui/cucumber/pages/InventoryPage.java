package com.forddirect.ui.cucumber.pages;

import com.forddirect.ui.cucumber.pages.framework.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * This class represents the inventory page
 */
public class InventoryPage extends BasePage {
    private static final Logger logger = LoggerFactory.getLogger( InventoryPage.class);

    @FindBy(xpath = "//span[@class='title']")
    private WebElementFacade inventoryTitle;

    @FindBy(xpath = "//a[@class='shopping_cart_link']")
    private WebElementFacade shoppingCart;

    @FindBy(xpath = "//div[@class='inventory_item_name']")
    private List<WebElementFacade> listOfInventoryItemNames;

    /**
     * Method to validate the user is on the inventory page
     */
    public Boolean isOnInventoryPage() {
        return doesElementExist( inventoryTitle );
    }

    public Boolean isShoppingCartLinkDisplayed(){
        return doesElementExist( shoppingCart );
    }

    public List<WebElementFacade> getListOfInventoryItemNames(){
        return listOfInventoryItemNames;
    }

}
