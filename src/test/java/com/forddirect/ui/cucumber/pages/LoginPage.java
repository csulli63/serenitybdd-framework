package com.forddirect.ui.cucumber.pages;

import com.forddirect.ui.cucumber.pages.framework.BasePage;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a login page
 */
public class LoginPage extends BasePage {
  private static final Logger logger = LoggerFactory.getLogger( LoginPage.class );

  /**
   * WebElement locators
   */
  @FindBy( id = "user-name" )
  private WebElementFacade usernameField;

  @FindBy( id = "password" )
  private WebElementFacade passwordField;

  @FindBy( id = "login-button" )
  private WebElementFacade loginButton;

  @FindBy( xpath = "//h3[@data-test='error']" )
  private WebElementFacade errorMessage;

  /**
   * LoginPage action using the provided username and password
   *
   * @param username
   * @param password
   */
  public void login( String username, String password ) {
    usernameField.type(username);
    passwordField.type( password );
    loginButton.click();
  }

  /**
   * Method that checks if the current url contains the specified text
   *
   * @param partialURL
   * @return
   */
  public boolean verifyLoginPageURL( String partialURL ) {
    return this.getDriver().getCurrentUrl().contains( partialURL );
  }

  /**
   * Method to get error message text
   */
  public String getErrorMessage() {
    return getElementText( errorMessage );
  }
}
