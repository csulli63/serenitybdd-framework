package com.forddirect.ui.cucumber;

import org.junit.runner.RunWith;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import io.cucumber.junit.CucumberOptions;

@RunWith( CucumberWithSerenity.class )
@CucumberOptions(
  features = { "src/test/resources/features/" },
  plugin = { "pretty" }
)
public class SerenityCucumberRunner {
}
