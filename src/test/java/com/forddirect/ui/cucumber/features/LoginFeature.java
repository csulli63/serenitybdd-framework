package com.forddirect.ui.cucumber.features;

import com.forddirect.ui.cucumber.steps.LoginSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;

@RunWith( SerenityRunner.class )
public class LoginFeature {

  @Managed( uniqueSession = true, clearCookies = ClearCookiesPolicy.BeforeEachTest )

  @Steps
  LoginSteps login;

  @Given( "user navigates to the login page" )
  public void userNavigatesToLoginPage() {
    login.navigateToLoginPage();
  }

  @Then( "user is on the login page containing the following url {string}" )
  public void userIsOnLoginPage( String partialURL ) {
    login.isOnLoginPage( partialURL );
  }

  @When( "user logs in with username {string} and password {string}" )
  public void userLogsInWithCredentials( String username, String password ) {
    login.logsIn( username, password );
  }

  @When( "user successfully authenticates" )
  public void userAuthenticates() {
    login.authenticate();
  }

  @Then( "invalid login message should be displayed {string}" )
  public void userLogsInWithInvalidCredentials( String errorMessage ) {
    login.verifyErrorMessage( errorMessage );
  }

}
