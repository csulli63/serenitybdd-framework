package com.forddirect.ui.cucumber.features;

import com.forddirect.ui.cucumber.steps.InventoryPageSteps;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.ClearCookiesPolicy;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RunWith( SerenityRunner.class )
public class InventoryPageFeature {
  private static final Logger logger = LoggerFactory.getLogger( InventoryPageFeature.class );

  @Managed( uniqueSession = true, clearCookies = ClearCookiesPolicy.BeforeEachTest )

  @Steps
  InventoryPageSteps inventoryPageSteps;

  @When( "user is on the inventory page" )
  public void userIsOnInventoryPage() {
    inventoryPageSteps.isOnInventoryPage();
  }

  @And( "shopping cart link is displayed" )
  public void shoppingCartLinkDisplayed() {
    inventoryPageSteps.isShoppingCartLinkDisplayed();
  }

  @And( "the number of items displayed in inventory should be {int}" )
  public void shoppingCartLinkDisplayed( int count ) {
    inventoryPageSteps.validateInventoryCount( count );
  }

  @And( "the inventory contains an item matching the following description:" )
  public void shoppingCartLinkDisplayed( DataTable dataTable ) {

    List<List<String>> dataTableList = dataTable.asLists( String.class );
    for ( List<String> tableLine : dataTableList ) {
      for ( String message : tableLine ) {
        System.out.println( message );
        inventoryPageSteps.validateInventoryItemNames( message );
      }
      System.out.println( tableLine );
    }

  }


}
