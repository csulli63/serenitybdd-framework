package com.forddirect.ui.cucumber.features;

import io.restassured.response.ValidatableResponse;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.rest.SerenityRest.given;

public class PetstoreFeature {

  @Step( "GET find by status" )
  public ValidatableResponse findByStatus( String status ) {
    return given()
      .relaxedHTTPSValidation()
      .header( "", "" )
      .when()
      .get( "https://petstore.swagger.io/v2/pet/findByStatus?status=" + status )
      .then();
  }

  @Step( "GET purchase order by id" )
  public ValidatableResponse findByPetID( String petID ) {
    return given()
      .relaxedHTTPSValidation()
      .header( "", "" )
      .when()
      .get( "https://petstore.swagger.io/v2/store/order/" + petID )
      .then();
  }

}
