package com.forddirect.ui.cucumber.steps;

import com.forddirect.ui.cucumber.features.PetstoreFeature;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

public class PetStoreSteps {

  @Steps
  PetstoreFeature petStore;

  @Given( "user executes a GET request to find pets by status {string}" )
  public void findByStatus( String status ) {
    petStore.findByStatus( status );
  }

  @Given( "user executes a GET request to find purchase order status by pet id {string}" )
  public void findByOrderByID( String petID ) {
    petStore.findByPetID( petID );
  }

  @Then( "status code is verified to be {int}" )
  public void verifyStatusCode( int statusCode ) {
    Assert.assertEquals( "Status Code does not match", SerenityRest.lastResponse().statusCode(), statusCode );
  }

  @Then( "purchase order status is verified to be {string}" )
  public void purchaseOrderStatus( String expectedOrderStatus ) {
    String actualOrderStatus = SerenityRest.lastResponse().getBody().jsonPath().getString( "status" );
    Assert.assertEquals( "Expected order status does not match", expectedOrderStatus, actualOrderStatus );
  }


}
