package com.forddirect.ui.cucumber.steps;

import com.forddirect.ui.cucumber.pages.LoginPage;
import com.forddirect.ui.cucumber.utils.EnvironmentProperties;
import net.thucydides.core.annotations.Step;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class LoginSteps {
  LoginPage loginPage;

  @Step
  public void logsIn( String username, String password ) {
    loginPage.login( username, password );
  }

  @Step
  public void authenticate() {
    String username = EnvironmentProperties.getPropertyValue( "ui.username" );
    String password = EnvironmentProperties.getPropertyValue( "ui.password" );
    loginPage.login( username, password );
  }

  @Step
  public void navigateToLoginPage() {
    loginPage.open();
  }

  @Step
  public void isOnLoginPage( String partialURL ) {
    assertTrue( loginPage.verifyLoginPageURL( partialURL ) );
  }

  @Step
  public void verifyErrorMessage( String errorMessage ) {
    assertEquals( "Invalid login message does not match expected ", errorMessage, loginPage.getErrorMessage() );
  }

}