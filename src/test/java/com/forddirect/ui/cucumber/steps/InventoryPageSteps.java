package com.forddirect.ui.cucumber.steps;

import com.forddirect.ui.cucumber.pages.InventoryPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class InventoryPageSteps {
  InventoryPage inventoryPage;

  @Step
  public void isOnInventoryPage() {
    Assert.assertTrue( "Inventory page title should be displayed", inventoryPage.isOnInventoryPage() );
  }

  @Step
  public void isShoppingCartLinkDisplayed() {
    Assert.assertTrue( "Shopping Cart link should be displayed", inventoryPage.isShoppingCartLinkDisplayed() );
  }

  @Step
  public void validateInventoryCount( int inventoryCount ) {
    Assert.assertEquals( "Inventory count does not match expected",
      inventoryPage.getListOfInventoryItemNames().size(), inventoryCount );
  }

  @Step
  public void validateInventoryItemNames( String inventoryItemName ) {
    List<WebElementFacade> listOfInventory = inventoryPage.getListOfInventoryItemNames();
    List<String> listOfInventoryItemNames = new ArrayList<>();

    for ( WebElementFacade webElementFacade : listOfInventory ) {
      listOfInventoryItemNames.add( webElementFacade.getText() );
    }

    Assert.assertTrue( "Inventory items do not contain name",
      listOfInventoryItemNames.contains( inventoryItemName ) );
  }

}
