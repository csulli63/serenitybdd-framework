package com.forddirect.ui.cucumber.utils;

import com.fasterxml.jackson.databind.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class JsonLoader {
  private static ObjectMapper mapper = new ObjectMapper();
  private static JSONParser parser = new JSONParser();

  /**
   * Parse a json file in JSON Array format and return a list of strings
   *
   * @param fileName
   * @return
   * @throws IOException
   * @throws ParseException
   */
  public static List<String> parseJSONFile( String fileName ) throws IOException, ParseException {
    return mapper.readValue( JsonLoader.loadJSONFile( fileName ), List.class );
  }

  private static String loadJSONFile( String fileName ) throws IOException, ParseException {
    Object obj = parser.parse( new FileReader( "src/test/resources/data/" + fileName ) );
    return mapper.writeValueAsString( obj );
  }


}
