Feature: Validate Inventory Content

  Background:
    Given user navigates to the login page
    And user is on the login page containing the following url "saucedemo"
    When user logs in with username "standard_user" and password "secret_sauce"
    Then user is on the inventory page

  @UI-Test
  Scenario: Validate inventory count
    When user is on the inventory page
    Then the number of items displayed in inventory should be 6

  @UI-Test
  Scenario: Validate inventory descriptions
    When user is on the inventory page
    Then the inventory contains an item matching the following description:
      | Sauce Labs Backpack               |
      | Sauce Labs Fleece Jacket          |
      | Sauce Labs Bolt T-Shirt           |
      | Test.allTheThings() T-Shirt (Red) |
      | Sauce Labs Onesie                 |