Feature: SwagLabs Login

  @UI-Test
  Scenario: Successful Login
    Given user navigates to the login page
    And user is on the login page containing the following url "saucedemo"
    When user logs in with username "standard_user" and password "secret_sauce"
    Then user is on the inventory page
    And shopping cart link is displayed

  @UI-Test
  Scenario: Login attempt using username password values from serenity conf properties
    Given user navigates to the login page
    And user is on the login page containing the following url "saucedemo"
    When user successfully authenticates
    Then user is on the inventory page

  @UI-Test
  Scenario: Invalid Login Attempt
    Given user navigates to the login page
    And user is on the login page containing the following url "sauce"
    When user logs in with username "problem_user" and password "forgot"
    Then invalid login message should be displayed "Invalid Login Try Again"