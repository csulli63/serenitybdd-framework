Feature: Validate Petstore API

  @API-Test
  Scenario: Validate find by order status
    Given user executes a GET request to find pets by status "available"
    Then status code is verified to be 200

  @API-Test
  Scenario: Find purchase order by pet ID and validate order status
    Given user executes a GET request to find purchase order status by pet id "3"
    Then status code is verified to be 200
    And purchase order status is verified to be "placed"

  @API-Test
  Scenario: Find purchase order by invalid pet ID and validate status code
    Given user executes a GET request to find purchase order status by pet id "069883"
    Then status code is verified to be 404